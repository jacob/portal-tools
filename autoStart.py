import emulab_sslxmlrpc.client.api as api
import emulab_sslxmlrpc.xmlrpc as xmlrpc
import json
import time
import random

def stillRunning(rpc):
    params = {
    "experiment" : "PowderSandbox,AutoRadarRX",
    "asjson"     : True
    }
    (exitval,response) = api.experimentStatus(rpc, params).apply();
    return(response.value != 12)

def runRadarRx():
    # Create the RPC server object.
    #print("Creating RPC Server")
    config = {
        "debug"       : 0,
        "impotent"    : 0,
        "verify"      : 0,
        "certificate" : ".ssl/emulab.pem",
    }
    rpc = xmlrpc.EmulabXMLRPC(config)

    #Define the experiment I would like to start
    while stillRunning(rpc):
        #print("Waiting for Experiment to Terminate")
        time.sleep(60)
   
    params = {
        "profile" : "NRDZ,radarRx",
        "proj"    : "PowderSandbox",
        "name"    : "AutoRadarRX",
        "paramset": "bill3,weekauto",
        "duration": "2"#Set duration incase terminate fails to free resources after 2 hours
    }
    (exitval,response) = api.startExperiment(rpc, params).apply();

    #Wait for the experiment to boot and Startup to Finish
    params = {
    "experiment" : "PowderSandbox,AutoRadarRX",
    "asjson"     : True
    }

    while True:
        (exitval,response) = api.experimentStatus(rpc, params).apply();
        status = json.loads(response.value)
        if status[u'status'] == "ready" and status[u'execute_status'][u'finished'] == 1:
            break
        if status[u'status'] == "failed":
            break
        #print("Waiting..." + status[u'status'])
        time.sleep(60)

    return


if __name__ == "__main__":
    midnight = time.time()
    # Generate 4 random times between 0:00 and 21:59
    times = sorted([random.randint(0, 22*60-1) for _ in range(4)])

    for t in times:
        # Convert minutes to seconds
        t_sec = t * 60
        # Get current time in seconds
        now = time.time()
        # Calculate the number of seconds until the next scheduled time
        delay = t_sec - (now - midnight)
        # Wait until the next scheduled time
        if delay >= 0:
            time.sleep(delay)
            runRadarRx()

