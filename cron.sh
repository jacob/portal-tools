#!/bin/bash

# Write the cronjob command to a temporary file
echo "0 0 * * * /usr/bin/python /local/repository/testtime.py" > /tmp/cronjob

# Install the cronjob from the temporary file
crontab /tmp/cronjob

# Remove the temporary file
rm /tmp/cronjob